import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import { HomeComponent } from './home/home.component';
import { CursosComponent } from './cursos/cursos.component';
import { ProfComponent } from './prof/prof.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent  } ,
  { path: 'home', component: HomeComponent  } ,
  { path: 'cursos', component: CursosComponent  } ,
  {path: 'prof', component: ProfComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
