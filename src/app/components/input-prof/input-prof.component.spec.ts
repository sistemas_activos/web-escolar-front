import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputProfComponent } from './input-prof.component';

describe('InputProfComponent', () => {
  let component: InputProfComponent;
  let fixture: ComponentFixture<InputProfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputProfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputProfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
