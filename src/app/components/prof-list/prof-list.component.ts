import { Component, OnInit,AfterViewInit, ViewChild  } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface UserData {
  id: string;
  nombre: string;
  apellido: string;
  mail: string;
  cursos: string;
}

const MAIL: string[] = [
  'prof1@mail.com', 'prof2@mail.com', 'prof3@mail.com', 'prof4@mail.com', 'prof5@mail.com', 'prof6@mail.com', 'prof7@mail.com', 'prof8@mail.com', 'prof9@mail.com', 'prof10@mail.com'
];
const NOMBRES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];
const APELLIDO: string[] = [
  'Perez', 'Allegrian','Soutullo', 'Vergara','Perez', 'Lopez'
];
const CURSO: string[] = [
  'Curso 1', 'Curso 2','Curso 3', 'Curso 4','Curso 5', 'Curso 6'
];

@Component({
  selector: 'app-prof-list',
  templateUrl: './prof-list.component.html',
  styleUrls: ['./prof-list.component.css']
})
export class ProfListComponent implements AfterViewInit {
  displayedColumns: string[] = ['id', 'nombre', 'apellido', 'mail', 'cursos'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
    // Create 10 users
    const users = Array.from({length: 10}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

/** Builds and returns a new User. */
function createNewUser(id: number): UserData {
  const nombre = NOMBRES[Math.round(Math.random() * (NOMBRES.length - 1))] 

  return {
    id: id.toString(),
    nombre: nombre,
    apellido: APELLIDO[Math.round(Math.random()*(APELLIDO.length -1))],
    mail: MAIL[Math.round(Math.random() * (MAIL.length - 1))],
    cursos: CURSO[Math.round(Math.random() * (CURSO.length - 1))]
  };
}