import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  cursosDisponibles: string[] = ['Curso1', 'Curso2', 'Curso3', 'Curso4', 'Curso5'];
  constructor() { }

  ngOnInit(): void {
  }

}
