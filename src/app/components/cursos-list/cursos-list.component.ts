import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

export interface UserData {
  id: string;
  nombre: string;
  nivel: string;
  descripcion: string;
}

/** Constants used to fill up our data base. */
const DESCRIPCION: string[] = [
  'Curso descripcion 1', 'Curso descripcion 2', 'Curso descripcion 3', 'Curso descripcion 4', 'Curso descripcion 5', 'Curso descripcion 6', 'Curso descripcion 7', 'Curso descripcion 8', 'Curso descripcion 9', 'Curso descripcion 10'
];
const NOMBRES: string[] = [
  'Curso 1', 'Curso 2', 'Curso 3', 'Curso 4', 'Curso 5', 'Curso 6', 'Curso 7', 'Curso 8', 'Curso 9', 'Curso 10'
];
const NIVEL: string[] = [
  'Inicial', 'Avanzado'
];

/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
  selector: 'app-cursos-list',
  templateUrl: './cursos-list.component.html',
  styleUrls: ['./cursos-list.component.css']
})
export class CursosListComponent implements AfterViewInit {
  displayedColumns: string[] = ['id', 'nombre', 'nivel', 'descripcion'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {
    // Create 10 users
    const users = Array.from({length: 10}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}

/** Builds and returns a new User. */
function createNewUser(id: number): UserData {
  const nombre = NOMBRES[Math.round(Math.random() * (NOMBRES.length - 1))] 

  return {
    id: id.toString(),
    nombre: nombre,
    nivel: NIVEL[Math.round(Math.random()*(NIVEL.length -1))],
    descripcion: DESCRIPCION[Math.round(Math.random() * (DESCRIPCION.length - 1))]
  };
}