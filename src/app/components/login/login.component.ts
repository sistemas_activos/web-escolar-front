import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { ShowHideStyleBuilder } from '@angular/flex-layout';
//import {Constants} from '@utils/constants';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
  })
  export class LoginComponent implements OnInit {

    public user: string;
    public password: string;
    public hide: boolean = true;


    public ngOnInit(): void {
    }

    public login() { 
      alert('El usuario: ' + this.user + ' no tiene permisos');
    }

  }